/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package digitalcamerasimulation;

/**
 *
 * @author DHAIRYA
 */
public class PointAndShootCamera {
    private String model;
    private String make;
    private String megapixel;
    private String memory;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getMegapixel() {
        return megapixel;
    }

    public void setMegapixel(String megapixel) {
        this.megapixel = megapixel;
    }

    public String getMemory() {
        return memory;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public PointAndShootCamera(String model, String make, String megapixel, String memory) {
        this.model = model;
        this.make = make;
        this.megapixel = megapixel;
        this.memory = memory;
    }
    
    
}
